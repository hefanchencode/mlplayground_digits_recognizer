#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 22:56:38 2020

@author: hefanchen
"""

# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot
import matplotlib as mpl
import pandas as pd


def show_image(image):
    """
    Render a given numpy.uint8 2D array of pixel data.
    """
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    imgplot = ax.imshow(image, cmap=mpl.cm.Greys)
    imgplot.set_interpolation('nearest')
    ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_ticks_position('left')
    pyplot.show()

def my_svm(C,eta,TOTAL,n1,n2,x,wno,bno,Num,count,N_col):
    tNum=count[n1,0]+count[n2,0] 
    xtrain=np.zeros((tNum,N_col))
    xtrain[:int(count[n1]) ]=x[n1,:int(count[n1])]
    xtrain[int(count[n1]):tNum]=x[n2,:int(count[n2])]
    #xtrain=x[n1,:count[n1]]+x[n2,:count[n2]]
    print(xtrain.shape)
    ttrain=np.zeros((tNum,1))
    ttrain[:int(count[n1]),0]=1
    ttrain[int(count[n1]):,0]=-1
              
    w=np.zeros((N_col,1))
    b=0
    for i in range(TOTAL):
        w_grad=w
        b_grad=0
        for j in range(tNum):
            if 1-ttrain[j]*(np.dot(xtrain[j],w)+b)>0:
                w_grad = w_grad - C*ttrain[j]*xtrain[j].reshape((N_col,1))
                b_grad = b_grad - C*ttrain[j]
        ai=eta/(i*eta+1)
        w=w-ai*w_grad
        b=b-ai*b_grad
    wno[n1,n2]=w.T
    bno[n1,n2]=b

# load training data
#trainLabels = np.array(np.loadtxt('train.csv', dtype=np.uint8, delimiter=','))
Data = np.array(np.loadtxt('train.csv', dtype=np.uint8, delimiter=',',skiprows=1))
trainData = Data[:,1:];
trainLabels = Data[:,0];
# trainData = pd.read_csv('train.csv',',');
# Visualize sample image
N_col = trainData.shape[1];
N_row = trainData.shape[0];
imgHeight = int(np.sqrt(N_col))
show_image(trainData[2].reshape((imgHeight, imgHeight)))

# load test data
testData = np.array(np.loadtxt('test.csv', dtype=np.uint8, delimiter=',',skiprows=1))
#%%
xtrain=np.zeros((10,N_row,N_col),dtype=np.uint8)
count=np.zeros((10,1),dtype=np.int)
for i in range(len(trainData)):
    no=trainLabels[i]
    xtrain[no,int(count[no])]=trainData[i]
    count[no]=count[no]+1
##trainning

w=np.zeros((10,10,N_col))
b=np.zeros((10,10,1))
eta=0.001
C=3
Num=len(trainData)
TOTAL=200
for n1 in range(10):
    for n2 in range(n1):
        my_svm(C,eta,TOTAL,n1,n2,xtrain,w,b,Num,count,N_col)
#%% 
file_output=open('predict_200.csv','w')
file_output.write('ImageId,Label\n')
#testing
for i in range(len(testData)):
    x=testData[i]
    tempcount=np.zeros((10,1))
    for n1 in range(10):
        for n2 in range(n1):
            m=np.dot(x.reshape(1,N_col),w[n1,n2].reshape(N_col,1))+b[n1,n2]
            if (m>0):
                tempcount[n1]=tempcount[n1]+1
            else:
                tempcount[n2]=tempcount[n2]+1
    no=tempcount.argmax(axis=0)
    file_output.write(str(i+1)+','+str(no[0])+'\n')
        
file_output.close()
            
