#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 23:32:21 2020

@author: hefanchen
"""


import numpy as np;
import time;

from sklearn import neighbors

time0 = time.time()
Data = np.array(np.loadtxt('train.csv', dtype=np.uint8, delimiter=',',skiprows=1))
trainData = Data[:,1:];
trainLabels = Data[:,0];

testData = np.array(np.loadtxt('test.csv', dtype=np.uint8, delimiter=',',skiprows=1))
print("Data Ready", time.time() - time0)

#%%
time0 = time.time()
clf = neighbors.KNeighborsClassifier(15, weights='distance');
clf.fit(trainData, trainLabels);
output = clf.predict(testData);

file_output=open('kNN2.csv','w')
file_output.write('ImageId,Label\n')
print("Solver Ready")
for i in range(len(testData)):
    #ans = output[i].argmax(axis=0)
    file_output.write(str(i+1)+','+str(output[i])+'\n')
        
file_output.close()
print ('k = 15 ', time.time() - time0)
