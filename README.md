# MLPlayground_digits_recognizer 

I played with a few classification algorithms provided by scikit-learn as well as my home-brew SVM code for the classical digits recognizer problem.

The data is from Kaggle. https://www.kaggle.com/c/digit-recognizer
