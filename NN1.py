
import numpy as np;
import time;
import os

from sklearn.neural_network import MLPClassifier

def testCase(caseId, layers):
	time0 = time.time()
	clf = MLPClassifier(solver='lbfgs', max_iter=400,activation='logistic', hidden_layer_sizes=layers, random_state=1);
	clf.fit(trainData, trainLabels);
	output = clf.predict(testData);

	file_output=open('NN%d.csv'%(caseId),'w')
	file_output.write('ImageId,Label\n')
	print("Solver Ready",end="")
	for i in range(len(testData)):
		ans = output[i].argmax(axis=0)
		file_output.write(str(i+1)+','+str(ans)+'\n')

	file_output.close()
	
	os.system("touch NeuralNetwork_log.txt")
	file_output=open('NeuralNetwork_log.txt','a')
	
	localtime = time.asctime(time.localtime(time.time()) )
	file_output.write('===' + localtime + '===\n')
	for i in layers: 
		file_output.write(str(i)+' ')

	file_output.write(':  time_elapse '+ str(time.time() - time0) + '\n\n')


time0 = time.time();
Data = np.array(np.loadtxt('train.csv', dtype=np.uint8, delimiter=',',skiprows=1))
trainData = Data[:,1:];
trainLabels = np.zeros((trainData.shape[0],10),dtype=np.uint8);
for i in range(trainData.shape[0]):
    trainLabels[i,Data[i,0]] = 1;
testData = np.array(np.loadtxt('test.csv', dtype=np.uint8, delimiter=',',skiprows=1))
print("Data Ready in",time.time() - time0,"s")
#%%

#testCase(1,(150,50))
#testCase(2,(100,100))
#testCase(3,(200,80))
#testCase(4,(300,100))
testCase(5,(400,))

